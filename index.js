const columnify = require('columnify');
const colors = require('colors');
const { program } = require('commander');
const serverService = require('./src/services/server');
const torrentService = require('./src/services/torrent');
const fs = require('fs');
const DATA_DIR = process.env.STORAGE;
const DATA_FILE = {
	SERVERS: 'servers.json',
};

program.version('0.0.1');

function columnifyTorrent(data) {
	return columnify(data, {
		truncate: true,
		columns: ['server', 'client', 'hash', 'name', 'length', 'downloaded', 'uploaded', 'ratio', 'active', 'createdAt', 'free_space'],
		config: {
			name: {maxWidth: 80},
			createdAt: {
				dataTransform: function(data) {
					const date = new Date(data);
					return `${date.getFullYear()}-${date.getMonth()}-${date.getDate()}`;
				}
			},
			length: {
				dataTransform: function(data) {
					return `${Math.round(data/(1024*1024*1024))} Go`
				},
			},
			downloaded: {
				dataTransform: function(data) {
					return `${Math.round(data/(1024*1024*1024))} Go`
				},
			},
			free_space: {
				dataTransform: function(data) {
					return `${Math.round(data/(1024*1024*1024))} Go`
				},
			},
			uploaded: {
				dataTransform: function(data) {
					return `${Math.round(data/(1024*1024*1024))} Go`
				},
			},
			ratio: {
				dataTransform: function(data) {
					if(data >= 1) {
						return `${data}`.green;
					} else if(data >= 0.8) {
						return `${data}`.yellow;
					}

					return `${data}`.red;
				}
			},
			active: {
				dataTransform: function(data) {
					if(data === 'true') {
						return `${data}`.green;
					}

					return `${data}`.red;
				}
			},
		}
	});
}

function columnifyServer(data) {
	return columnify(data, {
		truncate: true,
		columns: ['name', 'connected', 'client', 'host', 'port', 'endpoint', 'user', 'password'],
		config: {
			name: {maxWidth: 80},
			connected: {
				dataTransform: function(data) {
					if(data === 'true') {
						return 'Ok'.green;
					} else {
						return 'Nok'.red;
					}
				},
			}
		}
	});
}

function createDefaultDataFile() {
	try {
		const i = fs.accessSync(`${DATA_DIR}/${DATA_FILE.SERVERS}`, fs.constants.F_OK | fs.constants.W_OK | fs.constants.R_OK);
	} catch(e) {
		fs.writeFileSync(`${DATA_DIR}/${DATA_FILE.SERVERS}`, JSON.stringify([]), {
			encoding: 'utf8',
			flag: 'wx'
		});
	}
}

program
	.command('server:all')
	.description('Get all torrent server')
	.option("--connected", "Only server connected", 0)
	.action(async(options) => {
		try {
			const servers = serverService.getServers(options.connected);
			console.log(columnifyServer(servers));
		} catch(e) {
			console.log(`Error : ${e.message}`);
		}
	});

program
	.command('server:one <name>')
	.option("--connected", "Only server connected")
	.description('Get one torrent server')
	.action(async(name, options) => {
		try {
			const server = serverService.getServer(name, options.connected);
			console.log(columnifyServer([server]));
		} catch(e) {
			console.log(`Error : ${e.message}`);
		}
	});

program
	.command('server:add <name>')
	.description('Add torrent server')
	.requiredOption("-c, --client [client]", "Client of torrent server")
	.requiredOption("-h, --host [host]", "Host IP of server")
	.requiredOption("-p, --port [port]", "Port of server")
	.requiredOption("-e, --endpoint [endpoint]", "Endpoint of server")
	.option("-u, --user [user]", "Auth user")
	.option("-P, --password [password]", "Auth password")
	.action(async(name, options) => {
		try {
			await serverService.addServer({...options, name});
			console.log('Server added successfully')
		} catch(e) {
			console.log(`Unable to add server : ${name}`);
			console.log(`Error : ${e.message}`);
		}
	});

program
	.command('server:rm <name>')
	.description('Remove torrent server')
	.action(async(name) => {
		try {
			serverService.removeServer(name);
		} catch(e) {
			console.log(`Unable to remove server : ${name}`);
			console.log(`Error : ${e.message}`);
		}
	});

program
	.command('server:set <name> <key> <value>')
	.description('Add torrent server')
	.action(async(name, key, value) => {
		try {
			serverService.updateServerWithKeyValue(name, key, value);
			console.log('Server updated successfully')
		} catch(e) {
			console.log(`Unable to update server : ${name}`);
			console.log(`Error : ${e.message}`);
		}
	});

program
	.command('server:connect <name>')
	.description('Connect server')
	.option("-u, --user [user]", "Auth user")
	.option("-P, --password [password]", "Auth password")
	.action(async(name, options) => {
		try {
			const server = serverService.getServer(name, false);
			await serverService.connect({
				...server,
				user: options.user ?? server.user,
				password: options.password ?? server.password,
			});
			serverService.updateServerWithKeyValue(name, 'connected', true);
			if(options.user) {
				serverService.updateServerWithKeyValue(name, 'user', options.user);
				serverService.updateServerWithKeyValue(name, 'password', options.password);
			}
			console.log(`${name} is connected`);
		} catch(e) {
			serverService.updateServerWithKeyValue(name, 'connected', false);
			console.log(e.message);
		}
	});

program
	.command('server:disconnect <name>')
	.description('Disconnect server')
	.action(async(name) => {
		try {
			const server = serverService.getServer(name, false);
			serverService.updateServerWithKeyValue(name, 'connected', false);
			console.log(`${name} is disconnected`);
		} catch(e) {
			console.log(e.message);
		}
	});

program
	.command('torrent:all')
	.description('List all torrents')
	.option("--ratio [value]", "Get ratio superior of value", 0)
	.option("--active", "Get active torrent", 0)
	.option("--stopped", "Get stopped torrent", 0)
	.action(async(options) => {
		try {
			const servers = serverService.getServers(true);
			let torrents = [];
			for(let i=0; i<servers.length; i++) {
				try {
					torrents = torrents.concat((await torrentService.getFromServer(servers[i], {
						server: true,
						ratio: options.ratio,
						active: options.active,
						stopped: options.stopped,
					})));
				} catch(e) {
					console.log(e);
				}
			}
			console.log(columnifyTorrent(torrents.map((d) => ({...d, server: d.server.name}))))
		} catch(e) {
			console.log(e);
		}
	});

program
	.command('torrent:get <name>')
	.description('Get all torrent for server <name>')
	.option("--ratio [value]", "Get ratio superior of value", 0)
	.option("--active", "Get active torrent", 0)
	.option("--stopped", "Get stopped torrent", 0)
	.action(async(name, options) => {
		try {
			const server = serverService.getServer(name, true);
			const torrents = await torrentService.getFromServer(server, {
				server: true,
				ratio: options.ratio,
				active: options.active,
				stopped: options.stopped,
			});
			console.log(columnifyTorrent(torrents.map((d) => ({...d, server: d.server.name}))));
		} catch(e) {
			console.log(e);
		}
	});

program
	.command('torrent:one <name> <hash>')
	.description('Get one torrent')
	.action(async(name, hash) => {
		try {
			const server = serverService.getServer(name, true);
			const torrent = await torrentService.getOne(server, hash);
			console.log(columnifyTorrent([{
				server: server.name,
				client: server.client,
				...torrent,
			}]));
		} catch(e) {
			console.log(e);
		}
	});

program
	.command('torrent:pause <name> <hash>')
	.description('Pause one torrent')
	.action(async(name, hash) => {
		try {
			const server = serverService.getServer(name, true);
			await torrentService.pause(server, hash);
			console.log('Torrent paused successfully')
		} catch(e) {
			console.log(e);
		}
	});

program
	.command('torrent:play <name> <hash>')
	.description('Play one torrent')
	.action(async(name, hash) => {
		try {
			const server = serverService.getServer(name, true);
			await torrentService.play(server, hash);
			console.log('Torrent played successfully')
		} catch(e) {
			console.log(e);
		}
	});

program
	.command('torrent:rm <name> <hash>')
	.description('Remove one torrent')
	.action(async(name, hash) => {
		try {
			const server = serverService.getServer(name, true);
			await torrentService.remove(server, hash);
			console.log('Torrent removed successfully')
		} catch(e) {
			console.log(e);
		}
	});

program
	.command('torrent:files <name> <hash>')
	.description('Get files of one torrent')
	.action(async(name, hash) => {
		try {
			const server = serverService.getServer(name, true);
			const files = await torrentService.getFiles(server, hash);
			console.log(columnify(files, {
				config: {
					length: {
						dataTransform: (data) => {
							return `${Math.round(data/(1024*1024))} Mo`
						}
					},
					is_completed: {
						dataTransform: (data) => {
							return data === 'true' ? 'Ok'.green : 'NOk'.red;
						}
					}
				}
			}));
		} catch(e) {
			console.log(e);
		}
	});

program
	.command('torrent:add <name>')
	.description('Add torrent file to daemon')
	.option("-f, --file [file]", "Path of file")
	.action(async(name, options) => {
		try {
			const server = serverService.getServer(name, true);
			if(options.file) {
				await torrentService.createFromFile(server, options.file);
			} else {
				const getStdin = require('get-stdin');
				const bufferStdin = await getStdin.buffer();
				await torrentService.createFromBuffer(server, bufferStdin);
			}
			console.log('Torrent added successfully');
		} catch(e) {
			console.log(e);
		}
	});

program
	.command('server:listen')
	.option("-p, --port [port]", "Port of server", 8080)
	.action(async(options) => {
		createDefaultDataFile();
		try {
			const api = require('./src/app');
			await api.start(options.port);
		} catch(e) {
			console.log(`Fail start server: ${e.message}`);
			console.log(e);
		}
	});

program.parse(process.argv);
