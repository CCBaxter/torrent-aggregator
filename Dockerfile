FROM node:15-alpine
ENV DATA_DIR /var/data

RUN mkdir -p /var/data /var/app
RUN chown -R node:node /var/data /var/app
COPY --chown=node:node . /var/app

COPY entrypoint.sh /entrypoint.sh
RUN chmod u+x /entrypoint.sh

WORKDIR /var/app
VOLUME /var/data

EXPOSE 8080

ENTRYPOINT ["/entrypoint.sh"]
CMD ["server:listen"]