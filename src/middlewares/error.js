require('dotenv').config();
const ApiError = require('../class/ApiError');
const logger = require('../services/logger');

module.exports.handle = (err, req, res, next) => {
	let error = null;
	switch(err.name) {
		case 'ApiError':
			error = err;
			break;
		case 'NotFoundError':
			error = new ApiError(404, err.message, err.previous);
			break;
		case 'NotConnectedError':
			error = new ApiError(422, err.message, err.previous);
			break;
		case 'DuplicateError':
			error = new ApiError(422, err.message, err.previous);
			break;
		case 'FieldError':
			error = new ApiError(422, err.message, err.previous);
			break;
		default:
			error = new ApiError(err.status || 500, err.message, err);
	}

	logger.write(`[API] ${req.url} (${error.getStatus()}) - ${error.message}`, logger.LEVEL.ERROR);
	if(error.getStatus() >= 500 && error.getStatus() < 600) {
		logger.write(JSON.stringify(error), logger.LEVEL.ERROR);
	}
	res
		.status(error.getStatus())
		.type('json')
		.send(error.body())
	;
};