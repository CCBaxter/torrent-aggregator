const memcache = require('memory-cache');
const DURATION = 60*30; // 30 minutes

module.exports.cache = (req, res, next) => {
	const key = `torrent_${req.url}`;
	const body = memcache.get(key);
	const {forceCache} = req.query;
	if(forceCache) {
		next();
	} else {
		if(body) {
			res.send(body);
		} else {
			res.sendResponse = res.send;
			res.send = (body) => {
				memcache.put(key, body, 1000*DURATION);
				res.sendResponse(body);
			};
			next();
		}
	}
};