require('dotenv').config();
const path = require('path');
const fs = require('fs');
const moment = require('moment');

let logConfig = {
  mode: "console",
};

if(process.env.NODE_ENV === 'production') {
  logConfig = {
    mode: "files",
    directory: `${process.env.STORAGE}/logs`
  };

  if (!fs.existsSync(logConfig.directory)) {
    fs.mkdirSync(logConfig.directory);
  }
}

const LEVEL = {
  DEBUG: 1,
  WARN: 2,
  ERROR: 3,
  INFO: 4,
};

function getLevelString(level) {
  switch(level) {
    case LEVEL.ERROR:
      return 'ERROR';
    case LEVEL.INFO:
      return 'INFO';
    case LEVEL.WARN:
      return 'WARN';
    case LEVEL.DEBUG:
    default:
      return 'DEBUG';
  }
}

function write(message, level) {
  if(!level) {
    level = LEVEL.DEBUG;
  }

  const date = moment();
  const levelString = getLevelString(level);
  const completeMessage = `[${levelString}][${date.toISOString()}] : ${message}`;

  if(logConfig.mode === 'console') {
    console.log(completeMessage);
  } else if(logConfig.mode === 'files') {
    const logFile = path.join(logConfig.directory, `${date.format('YYYYMMDD')}_${levelString.toLowerCase()}.log`);

    if(!fs.existsSync(logFile)) {
      fs.writeFile(
        logFile,
        `${completeMessage}\n`,
        'utf8',
        (err) => {
          if(err) {
            console.log('Logger write errors');
            console.log(err);
          }
        }
      );
    } else {
      fs.appendFile(
        logFile,
        `${completeMessage}\n`,
        'utf8',
        (err) => {
          if(err) {
            console.log('Logger append errors');
            console.log(err);
          }
        }
      );
    }
  }
}

module.exports = {
  LEVEL,
  write,
};