require('dotenv').config();
const express = require('express');
const router = require('express-imp-router');
const bodyParser = require('body-parser');
const path = require('path');
const logger = require('./services/logger');

const app = express();
router(app);
if(process.env.NODE_ENV === 'development') {
	router.enableDebug();
}

router.route([{
	controllers: `${path.resolve('.')}/src/controllers`,
	middlewares: `${path.resolve('.')}/src/middlewares`,
	routes: {
		[router.IMP.MIDDLEWARE]: [
			{
				controllers: [bodyParser.json(), bodyParser.urlencoded({extended: true})],
				level: router.MIDDLEWARE.LEVEL.GLOBAL
			},
			{
				controllers: ['error#handle'],
				level: router.MIDDLEWARE.LEVEL.ERROR,
				inheritance: router.MIDDLEWARE.INHERITANCE.DESC,
			},
			{
				controllers: ['jwt#jwt'],
				inheritance: router.MIDDLEWARE.INHERITANCE.DESC,
			}
		],
		'/servers': {
			[router.IMP.MIDDLEWARE]: [
				{
					controllers: ['jwt#jwt'],
					inheritance: router.MIDDLEWARE.INHERITANCE.DESC,
				}
			],
			get: 'ServerController#get',
			post: 'ServerController#add',
			'/test': {
				post: 'ServerController#test',
			},
			'/:name': {
				get: 'ServerController#getOne',
				patch: 'ServerController#update',
				delete: 'ServerController#delete',
				'/upload': {
					[router.IMP.MIDDLEWARE]: {
						controllers: ['upload#torrent'],
						method: router.METHOD.POST,
						inheritance: router.MIDDLEWARE.INHERITANCE.NONE,
					},
					post: 'ServerController#upload'
				},
				'/connect': {
					get: 'ServerController#connectOne',
				},
				'/disconnect': {
					get: 'ServerController#disconnectOne',
				},
				'/torrents': {
					get: 'TorrentController#getFromServer',
					'/:hash': {
						get: 'TorrentController#getOne',
						patch: 'TorrentController#update',
						delete: 'TorrentController#delete',
						'/files': {
							get: 'TorrentController#getFiles',
						}
					},
				}
			},
		},
		'/torrents': {
			[router.IMP.MIDDLEWARE]: [
				{
					controllers: ['cache#cache'],
					inheritance: router.MIDDLEWARE.INHERITANCE.DESC,
					method: router.METHOD.GET,
				},
				{
					controllers: ['jwt#jwt'],
					inheritance: router.MIDDLEWARE.INHERITANCE.DESC,
				}
			],
			get: 'TorrentController#getAll',
		}
	}
}]);

module.exports.start = async(port=8080) => {
	try {
		await app.listen(port);
		logger.write(`Server started at port: ${port}`, logger.LEVEL.INFO);
	} catch(e) {
		logger.write(`Fail start API: ${e.message}`, logger.LEVEL.ERROR);
	}
};