class DuplicateError extends Error {
  constructor(message, err) {
    super(message);
    this.name = 'DuplicateError';
    this.previous = {
      message: err ? err.message : '',
    };
  }

  getPrevious() {
    return this.previous;
  }
}

module.exports = DuplicateError;