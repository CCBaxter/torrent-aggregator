const torrentService = require('../services/torrent');
const serverService = require('../services/server');
const ApiError = require('../class/ApiError');

/**
 * Get all torrents
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
module.exports.getAll = async(req, res, next) => {
	try {
		const {ratio, active, stopped, server, files} = req.query;
		const servers = serverService.getServers(true);
		let torrents = [];
		for(let i=0; i<servers.length; i++) {
			try {
				torrents = torrents.concat((await torrentService.getFromServer(servers[i], {ratio, active, stopped, server, files})));
			} catch(e) {}
		}
		res.send(torrents);
	} catch(e) {
		next(e);
	}
};

/**
 * Get all torrents for one server
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
module.exports.getFromServer = async(req, res, next) => {
	try {
		const {details, ratio, active, stopped, server, files} = req.query;
		const {name} = req.params;
		const _server = serverService.getServer(name, true);
		let torrents;
		if(details) {
			torrents = await torrentService.getFromServer(_server, {ratio, active, stopped, server, files});
		} else {
			torrents = await torrentService.getHashesFromServer(_server);
		}

		res.send(torrents);
	} catch(e) {
		next(e);
	}
}

/**
 * Get one torrent
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
module.exports.getOne = async(req, res, next) => {
	try {
		const {files} = req.query;
		const {hash, name} = req.params;

		if(!name) {
			throw new ApiError(400, 'Name is missing');
		}

		if(!hash) {
			throw new ApiError(400, 'Hash is missing');
		}

		const server = serverService.getServer(name, true);
		const torrents = await torrentService.getOne(server, hash, files === '1');
		res.send(torrents);
	} catch(e) {
		next(e);
	}
};

/**
 * Get files from torrent
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
module.exports.getFiles = async(req, res, next) => {
	try {
		const {hash, name} = req.params;

		if(!name) {
			throw new ApiError(400, 'Name is missing');
		}

		if(!hash) {
			throw new ApiError(400, 'Hash is missing');
		}

		const server = serverService.getServer(name, true);
		const files = await torrentService.getFiles(server, hash);
		res.send(files);
	} catch(e) {
		next(e);
	}
};

/**
 * Update one torrents
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
module.exports.update = async(req, res, next) => {
	try {
		const {action} = req.query;
		const {hash, name} = req.params;

		if(!name) {
			throw new ApiError(400, 'Name is missing');
		}

		if(!hash) {
			throw new ApiError(400, 'Hash is missing');
		}

		if(!['play','pause'].includes(action)) {
			throw new ApiError(400, 'This action is not available');
		}

		const server = serverService.getServer(name, true);
		if(action === 'play') {
			await torrentService.play(server, hash);
		} else if(action === 'pause') {
			await torrentService.pause(server, hash);
		}

		const torrent = await torrentService.getOne(server, hash);
		res.send(torrent);
	} catch(e) {
		next(e);
	}
};

/**
 * Delete one torrent
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
module.exports.delete = async(req, res, next) => {
	try {
		const {hash, name} = req.params;

		if(!name) {
			throw new ApiError(400, 'Name is missing');
		}

		if(!hash) {
			throw new ApiError(400, 'Hash is missing');
		}

		const server = serverService.getServer(name, true);
		await torrentService.remove(server, hash);
		res.send({
			success: true,
		});
	} catch(e) {
		next(e);
	}
};
