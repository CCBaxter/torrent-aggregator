const serverService = require('../services/server');
const torrentService = require('../services/torrent');
const ApiError = require('../class/ApiError');
const NotConnectedError = require('../class/NotConnectedError');
const wTorrent = require('wtorrent');

/**
 * Get all servers
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
module.exports.get = async(req, res, next) => {
	try {
		const {connected} = req.query;
		const servers = serverService.getServers(!!connected);
		res.send(servers);
	} catch(e) {
		next(e);
	}
};

/**
 * Get one server
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
module.exports.getOne = async(req, res, next) => {
	try {
		const {connected} = req.query;
		const {name} = req.params;
		const server = serverService.getServer(name, !!connected);
		res.send(server);
	} catch(e) {
		next(e);
	}
};

/**
 * Add server
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
module.exports.add = async(req, res, next) => {
	try {
		const {connect} = req.query;
		const {name, client, host, port, endpoint, user, password} = req.body;

		if(!client) {
			throw new ApiError(400, 'Client is missing', null);
		}

		if(!name) {
			throw new ApiError(400, 'Name is missing', null);
		}

		const server = await serverService.addServer({name, client, host, port, endpoint, user, password}, !(connect === '1'));
		res.send(server);
	} catch(e) {
		next(e);
	}
};

/**
 * Connect one server
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
module.exports.connectOne = async(req, res, next) => {
	try {
		const {name} = req.params;
		const server = serverService.getServer(name);
		await serverService.connect(server);
		res.send(server);
	} catch(e) {
		next(e);
	}
};

/**
 * Disconnect one server
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
module.exports.disconnectOne = async(req, res, next) => {
	try {
		const {name} = req.params;
		const server = serverService.getServer(name);
		serverService.updateServerWithKeyValue(server.name, 'connected', false);
		res.send(server);
	} catch(e) {
		next(e);
	}
};

/**
 * Update one server
 * @param req
 * @param res
 * @param next
 * @returns {Promise<*>}
 */
module.exports.update = async(req, res, next) => {
	try {
		const {connect} = req.query;
		const {client, name, host, port, endpoint, user, password} = req.body;

		const server = serverService.getServer(req.params.name);
		const newServer = serverService.updateServer(server.name, {client, name, host, port, endpoint, user, password});
		if(connect === '1') {
			await serverService.connect(newServer);
		}

		res.send(newServer);
	} catch(e) {
		next(e);
	}
};

/**
 * Delete one server
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
module.exports.delete = async(req, res, next) => {
	try {
		const {name} = req.params;
		serverService.removeServer(name);
		res.send({
			message: 'success'
		});
	} catch(e) {
		next(e);
	}
};

/**
 * Upload torrent file
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
module.exports.upload = async(req, res, next) => {
	try {
		const {name} = req.params;
		const torrent = req.file;
		if(!torrent) {
			throw new ApiError(422, 'You should upload a torrent file');
		}
		const server = serverService.getServer(name, false);
		await torrentService.createFromFile(server, torrent.path);
		res.send({
			message: 'success',
		});
	} catch(e) {
		next(e);
	}
};

/**
 * Test connection
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
module.exports.test = async(req, res, next) => {
	try {
		serverService.checkServerFields(req.body);
		try {
			await wTorrent(req.body);
		} catch(e) {
			throw new NotConnectedError('Server cannot be connect', e);
		}

		res.send({
			message: 'Connected'
		})
	} catch(e) {
		next(e);
	}
};